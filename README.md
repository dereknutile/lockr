# Lockr

## About

Secrets locker built in [Laravel](https://laravel.com/) with [Livewire](https://laravel-livewire.com/).

## Develop

    $ cd lockr
    $ php artisan serve

URL: [http://localhost:8000/](http://localhost:8000/)