<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuditVaultController;
use App\Http\Controllers\VaultController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*
|-------------------------------------------------------------------------------
| Place routes that need AUTHENTICATION here
|-------------------------------------------------------------------------------
*/
Route::group(['middleware' => ['auth:sanctum', 'verified']], function () {
    // Dashboard
    Route::get('dashboard', function () {
        return view('dashboard');
    })->name('dashboard');

    // Audit routes
    Route::get('audit', [AuditVaultController::class,'index'])->name('audit');
    
    // Vault routes
    Route::get('vault', [VaultController::class,'index'])->name('vault');
    Route::get('vault/create', [VaultController::class,'create'])->name('vault.create');
    Route::get('vault/unlock/{uuid}', [VaultController::class,'show'])->name('vault.unlock');
    Route::put('vault', [VaultController::class,'update'])->name('vault.update');
    Route::post('vault', [VaultController::class,'store'])->name('vault.store');
});
