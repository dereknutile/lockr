<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Vault') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            @if(isset($uuid))
                @livewire($view, ['uuid' => $uuid])
            @else
                @livewire($view)
            @endif
        </div>
    </div>
</x-app-layout>