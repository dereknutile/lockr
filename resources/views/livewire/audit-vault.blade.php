<div class="bg-white overflow-hidden shadow-xl sm:rounded-lg p-4">
    @if($entries && count($entries)>0)
    <table class="table-auto w-full">
        <thead>
            <tr>
                <th class="border p-2 bg-gray-800 text-white text-left">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($entries as $entry)
            <tr>
                <td class="border p-2 text-left"><strong>{{ $entry->action }}</strong></td>
            </tr>
            @endforeach
        </tbody>
    </table>
    @else
    <p>You have no entries in the vault audit.</p>
    @endif
</div>
