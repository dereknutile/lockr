<div class="bg-white overflow-hidden shadow-xl sm:rounded-lg p-4">
    <form action="{{ route('vault.store') }}" method="post" class="">
        @csrf

        <input type="hidden" name="user_id" value="{{ auth()->user()->id }}">
        
        <div class="form-group">
            <label for="name" class="block font-medium text-sm text-gray-700 font-bold">Name *</label>
            <input type="text" class="form-input rounded-md shadow-sm block mt-1 w-full" name="name" value="{{ old('name') }}" placeholder="Name of secret">
        </div>
        
        <div class="form-group">
            <label for="description" class="block font-medium text-sm text-gray-700">Description</label>
            <textarea class="form-input rounded-md shadow-sm block mt-1 w-full" name="description" placeholder="Optional description of secret"></textarea>
        </div>
        
        <div class="form-group">
            <label for="username" class="block font-medium text-sm text-gray-700">Username</label>
            <input type="text" class="form-input rounded-md shadow-sm block mt-1 w-full" name="username" value="{{ old('username') }}" placeholder="Optionally include a username">
        </div>
        
        <div class="form-group">
            <label for="secret" class="block font-medium text-sm text-gray-700 font-bold">Secret *</label>
            <input type="text" class="form-input rounded-md shadow-sm block mt-1 w-full" name="secret" value="{{ old('secret') }}" placeholder="Add your secret, password, or key here, we'll encrypt it">
        </div>

        <div class="flex items-center justify-end mt-4">
            <a href="{{ route('vault') }}" class="btn btn-link">cancel</a>
            <input type="submit" class="inline-flex items-center px-4 py-2 bg-gray-800 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150 ml-4" name="" value="Save and Submit">
        </div>
        <!-- /.form-actions -->
    </form>
</div>
