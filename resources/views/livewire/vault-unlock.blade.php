<div>
    <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg p-4">
        <form action="{{ route('vault') }}" method="post" class="">
            @method('put')
            @csrf

            <input type="hidden" name="user_id" value="{{ auth()->user()->id }}">
            <input type="hidden" name="uuid" value="{{ $secret->uuid }}">
            
            <div class="form-group">
                <label for="name" class="block font-medium text-sm text-gray-700 font-bold">Name *</label>
                <input type="text" class="form-input rounded-md shadow-sm block mt-1 w-full" name="name" value="{{ $secret->name }}" placeholder="Name of secret">
            </div>
            
            <div class="form-group">
                <label for="description" class="block font-medium text-sm text-gray-700">Description</label>
                <textarea class="form-input rounded-md shadow-sm block mt-1 w-full" name="description" placeholder="Optional description of secret">{{ $secret->description }}</textarea>
            </div>
            
            <div class="form-group">
                <label for="username" class="block font-medium text-sm text-gray-700">Username</label>
                <input type="text" class="form-input rounded-md shadow-sm block mt-1 w-full" name="username" value="{{ $secret->username }}" placeholder="Optionally include a username">
            </div>
            
            <div class="form-group">
                <label for="secret" class="block font-medium text-sm text-gray-700 font-bold">Secret *</label>
                <input type="text" class="form-input rounded-md shadow-sm block mt-1 w-full" name="secret" value="{{ $secret->decryptSecret($secret->secret) }}" placeholder="Add your secret, password, or key here, we'll encrypt it">
            </div>

            <div class="flex items-center justify-end mt-4">
                <a href="{{ route('vault') }}" class="btn btn-link">cancel</a>
                <input type="submit" class="inline-flex items-center px-4 py-2 bg-gray-800 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150 ml-4" name="" value="Save and Submit">
            </div>
            <!-- /.form-actions -->
        </form>
    </div>

    <div>
        @if(count($audits)>0)
            <div class="bg-gray-400 overflow-hidden shadow-xl sm:rounded-lg p-4 mt-4">
                <h5 class="font-bold text-lg text-gray-800 leading-tight mb-2">Recent Audit Trial ({{ count($audits) }})</h5>

                <table class="table-auto w-full bg-gray-200">
                    <thead>
                        <tr>
                            <th class="border p-2 bg-gray-800 text-white w-1/3">User</th>
                            <th class="border p-2 bg-gray-800 text-white w-1/3">Action</th>
                            <th class="border p-2 bg-gray-800 text-white w-1/3 text-right">Date (UTC)</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($audits as $entry)
                        {{-- Highlight any access that isn't this user --}}
                        @if($entry->user->id !== auth()->user()->id)
                        <tr class="bg-yellow-100">
                        @else
                        <tr>
                        @endif
                            <td class="border p-2 text-left">{{ $entry->user->name }}</td>
                            <td class="border p-2 text-left">{{ $entry->action }}</td>
                            <?php 
                            $date = new \Carbon\Carbon($entry->created_at, 'UTC');
                            $date->setTimezone('America/Los_Angeles');
                            ?>
                            <td class="border p-2 text-right">{{ $date }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @endif
    </div>
</div>