<div class="bg-white overflow-hidden shadow-xl sm:rounded-lg p-4">

    <div class="mb-6 text-right">
        <a href="{{ route('vault.create') }}" class="inline-flex items-center px-4 py-2 bg-gray-800 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150 ml-4">Add a Secret</a>
    </div>
    @if($secrets && count($secrets)>0)
    <table class="table-auto w-full">
        <thead>
            <tr>
                <th class="border p-2 bg-gray-800 text-white text-left">Name</th>
                <th class="border p-2 bg-gray-800 text-white text-left">Description</th>
                <th class="border p-2 bg-gray-800 text-white w-32">Actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach($secrets as $secret)
            <tr>
                <td class="border p-2 text-left"><strong>{{ $secret->name }}</strong></td>
                <td class="border p-2 text-left">{{ $secret->description }}</td>
            <td class="border p-2 text-right"><a href="{{ route('vault.unlock',$secret->uuid) }}" class="text-blue-600 underline">unlock</a> | <a href="" class="text-blue-600 underline">delete</a></td>
            </tr>
            @endforeach
        </tbody>
    </table>
    @else
    <p>You have no secrets in the vault.</p>
    @endif
</div>
