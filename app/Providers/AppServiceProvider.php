<?php

namespace App\Providers;

use App\Observers\VaultObserver;
use Illuminate\Support\ServiceProvider;
use App\Models\Vault;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Vault::observe(VaultObserver::class);
    }
}
