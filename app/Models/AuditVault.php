<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AuditVault extends Model
{
    use HasFactory;

    /**
     * User relationship
     */
    public function user()
    {
        return $this->hasOne('App\Models\User','id','user_id');
    }

    /**
     * Vault relationship
     */
    public function vaults()
    {
        return $this->hasOne('App\Models\Vault','id','vault_id');
    }
}
