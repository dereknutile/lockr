<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\Crypt;

class Vault extends Model
{
    use HasFactory;

    // Note: Eloquent makes the assumption that the primary key (which is named
    // id by default) is an integer, and it casts it to int by default in the 
    // getCasts method
    protected $casts = [
        'uuid' => 'string'
    ];

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    // protected $primaryKey = 'uuid';

    /**
     * Audit relationship
     */
    public function audits()
    {
        return $this->hasMany('App\Models\AuditVault');
        // return $this->hasMany('App\Models\AuditVault', 'vault_id', 'id');
    }

    /**
     * Encrypt a string
     *
     * @param  string $string
     * @return Response
     */
    public function encryptSecret($string)
    {
        return Crypt::encryptString($string);
    }

    /**
     * Store a secret message for the user.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function decryptSecret($string)
    {
        try {
            return Crypt::decryptString($string);
        } catch (DecryptException $e) {
            return $e;
        }
    }
}
