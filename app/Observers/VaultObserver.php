<?php

namespace App\Observers;

use App\Models\AuditVault;
use App\Models\Vault;

class VaultObserver
{
    /**
     * Handle the Vault "created" event.
     *
     * @param  \App\Models\Vault  $vault
     * @return void
     */
    public function created(Vault $vault)
    {
        //
    }

    /**
     * Handle the Vault "updated" event.
     *
     * @param  \App\Models\Vault  $vault
     * @return void
     */
    public function updated(Vault $vault)
    {
        $audit = new AuditVault();
        $audit->vault_id = $vault->id;
        $audit->user_id = auth()->user()->id;
        $audit->action = 'updated';
        $audit->save();
    }

    /**
     * Handle the Vault "deleted" event.
     *
     * @param  \App\Models\Vault  $vault
     * @return void
     */
    public function deleted(Vault $vault)
    {
        //
    }

    /**
     * Handle the Vault "restored" event.
     *
     * @param  \App\Models\Vault  $vault
     * @return void
     */
    public function restored(Vault $vault)
    {
        //
    }

    /**
     * Handle the Vault "force deleted" event.
     *
     * @param  \App\Models\Vault  $vault
     * @return void
     */
    public function forceDeleted(Vault $vault)
    {
        //
    }
}
