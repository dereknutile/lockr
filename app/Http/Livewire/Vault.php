<?php

namespace App\Http\Livewire;

use App\Models\Vault as Secret;
use Livewire\Component;

class Vault extends Component
{
    public $secrets;

    public function mount()
    {
        $this->secrets = Secret::orderBy('created_at','desc')->get();
    }

    public function render()
    {
        return view('livewire.vault');
    }
}
