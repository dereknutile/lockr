<?php

namespace App\Http\Livewire;

use App\Models\AuditVault;
use App\Models\Vault as Secret;
use Livewire\Component;

class VaultUnlock extends Component
{
    public $audits;
    public $secret;
    public $uuid;

    public function mount($uuid)
    {
        $this->uuid = $uuid;
        // Get this secret
        $this->secret = Secret::where('uuid',$uuid)->first();
        // We touch this model to trigger the updated observable for auditing
        $this->secret->touch();

        // Gather audit trial for this secret
        $this->audits = AuditVault::with('user')
            ->where('vault_id',$this->secret->id)
            ->orderBy('created_at','desc')
            ->limit(100)
            ->get();
    }

    public function render()
    {
        return view('livewire.vault-unlock');
    }
}
