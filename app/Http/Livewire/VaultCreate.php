<?php

namespace App\Http\Livewire;

use Livewire\Component;

class VaultCreate extends Component
{
    public function render()
    {
        return view('livewire.vault-create');
    }
}
