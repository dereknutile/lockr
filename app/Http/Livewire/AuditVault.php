<?php

namespace App\Http\Livewire;

use App\Models\AuditVault as Audit;
use Livewire\Component;

class AuditVault extends Component
{
    public $entries;

    public function mount()
    {
        $this->entries = Audit::where('user_id',auth()->user()->id)
            ->orderBy('created_at','desc')
            ->get();
    }

    public function render()
    {
        return view('livewire.audit-vault');
    }
}
