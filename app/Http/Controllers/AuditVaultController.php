<?php

namespace App\Http\Controllers;

use App\Models\AuditVault;
use App\Models\Vault;
use Illuminate\Http\Request;

class AuditVaultController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vaults = Vault::where('user_id',auth()->user()->id);
        $audit = AuditVault::with('vaults')->get();
        dd($audit);
        $view = 'audit-vault';
        return view('vault',compact('view'));
    }
}
