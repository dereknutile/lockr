<?php

namespace App\Http\Controllers;

use App\Models\Vault;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class VaultController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $view = 'vault';
        return view('vault',compact('view'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $view = 'vault-create';
        return view('vault',compact('view'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'user_id' => 'required|numeric',
            'name' => 'required|max:255',
            'secret' => 'required'
        ]);

        $vault = new Vault;
        $vault->uuid = Str::orderedUuid();
        $vault->user_id = $request->user_id;
        $vault->name = $request->name;
        $vault->description = $request->description;
        $vault->username = $request->username;
        $vault->secret = $vault->encryptSecret($request->secret);
        if($vault->save()){
            session()->flash('flash_success','Secret saved');
        } else {
            session()->flash('flash_error','Unable to save secret');
        }
        return redirect()->route('vault');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Vault  $vault
     * @return \Illuminate\Http\Response
     */
    public function show($uuid)
    {
        $view = 'vault-unlock';
        return view('vault',compact('uuid','view'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Vault  $vault
     * @return \Illuminate\Http\Response
     */
    public function edit(Vault $vault)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Vault  $vault
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Vault $vault)
    {
        $request->validate([
            'user_id' => 'required|numeric',
            'uuid' => 'required',
            'name' => 'required|max:255',
            'secret' => 'required'
        ]);

        $vault = Vault::where('uuid',$request->uuid)->first();
        $vault->uuid = Str::orderedUuid();
        $vault->user_id = $request->user_id;
        $vault->name = $request->name;
        $vault->description = $request->description;
        $vault->username = $request->username;
        $vault->secret = $vault->encryptSecret($request->secret);
        if($vault->save()){
            session()->flash('flash_success','Secret updated');
        } else {
            session()->flash('flash_error','Unable to update secret');
        }
        return redirect()->route('vault');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Vault  $vault
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vault $vault)
    {
        //
    }
}
