<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use App\Models\Vault;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Crypt;

class VaultFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Vault::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'uuid' => Str::uuid(),
            'user_id' => 1,
            'name' => $this->faker->word(),
            'description' => $this->faker->paragraph,
            'secret' => Crypt::encryptString($this->faker->word)
        ];
    }
}
