<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVaultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // A bank vault is a secure space where money, valuables, records, and 
        // documents are stored. It is intended to protect their contents from 
        // theft, unauthorized use, fire, natural disasters, and other threats, 
        // much like a safe.
        Schema::create('vaults', function (Blueprint $table) {
            $table->id();
            // Using a unique UUID rather than an ID
            $table->uuid('uuid')->unique();
            $table->foreignId('user_id')->index();
            $table->string('name');
            $table->text('description')->nullable();
            $table->string('username')->nullable();
            // This is the encrypted secret
            $table->string('secret');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vaults');
    }
}
